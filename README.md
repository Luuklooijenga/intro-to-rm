# intro-to-rm

This is the repository for introduction to research methods.

In order for you to run the scripts provided there are two datasets you need to collect. 

1. https://www.kaggle.com/therohk/global-news-week/home#news-week-18aug24.csv
2. https://github.com/aesuli/sentiwordnet

From the first download we are usinig the news-week-18aug24.csv file.
After downloading and unpacking both datasets, you can run the run.sh script in order for it to automatically run everything.

In the analysis.html file you can see the statistical tests that were described in the paper.
