from collections import Counter
import pandas as pd
import re


def split_media():

    """ Splits media into biased and unbiased media according to media outlet """

    biased_list = list()
    unbiased_list = list()

    with open("data.txt", "r") as infile:
        for line in infile:
            line = line.split("\t")
            if line[0] == "w3-usatoday" or line[0] == "w3-bbc" or line[0] == "w3-npr-org":
                s = line[1][:-1].lower()
                s = re.sub(r'[^\w\s]','',s)
                unbiased_list.append((line[0], s))
            else:
                s = line[1][:-1].lower()
                s = re.sub(r'[^\w\s]','',s)
                biased_list.append((line[0], s))
            
    # Remove duplicates from list
    biased = list(dict.fromkeys(biased_list))
    unbiased = list(dict.fromkeys(unbiased_list))

    # Make biased and unbiased equal length by removing headlines from the news outlet with the most data
    ny = list()
    biased_v2 = list()
    for i in biased:
        if i[0] == "w3-nydailynews":
            ny.append(i)
        else:
            biased_v2.append(i)
    c = -1
    for i in range(len(unbiased)-len(biased_v2)):
        c += 1
        biased_v2.append(ny[c])

    return biased_v2, unbiased

def read_swn():

    # Dictionary structure: word : (pos, neg)
    swn_dict = dict()

    with open("SentiWordNet_3.0.0.txt", "r") as infile:
        for line in infile:
            line = line.split("\t")
            if len(line) == 6:
                word = line[4].split()
                if len(word) > 1:
                    for i in word:
                        swn_dict[i[:-2]] = (float(line[2]), float(line[3]))
                else:
                    try:
                        swn_dict[word[0][:-2]] = (float(line[2]), float(line[3]))
                    except ValueError:
                        continue

    return swn_dict

def score(headlines, swn_dict):
    
    """ This function creates a score for every headline in the dataset, see paper for method """

    c = 0
    scores = list()

    for i in headlines:
        c += 1
        name = i[0]
        tokens = i[1].split()
        polarity_list = list()
        for t in tokens:
            try:
                polarity = swn_dict[t]
                polarity_list.append(polarity)
            
            except KeyError:
                continue
        
        if polarity_list != []:
            scoring_list = list()
            for p in polarity_list:
                if p[0] + p[1] != 0.0:
                    s = 1 - (p[0] + p[1])
                    scoring_list.append(s)
            
            if scoring_list == []:
                scores.append((name, 1))
            elif len(scoring_list) == 1:
                scores.append((name, scoring_list[0]))
            else:
                total = 0
                for i in scoring_list:
                    total = total + i
                normalized_score = total/len(scoring_list)
                scores.append((name, normalized_score))
        else:
            scores.append((name, 1))
    
    return scores

def scores_per_medium(scores):

    """ This function provides the average score per medium """

    score_dict = dict()
    for i in scores:
        try:
            score_dict[i[0]] += i[1]
        except KeyError:
            score_dict[i[0]] = i[1]
        
    counts = list(Counter(x[0] for x in scores).items())

    for i in counts:
        score = (score_dict[i[0]] / i[1])
        print(i[0], score)


def main():

    b_list, ub_list = split_media()
    swn_dict = read_swn()

    biased_media_scores = score(b_list, swn_dict)
    unbiased_media_scores = score(ub_list, swn_dict)

    print("average subjectivity score per medium")
    print("-------------------------------------")
    scores_per_medium(biased_media_scores)    
    scores_per_medium(unbiased_media_scores)
    
    labels = ["Number", "Score"]
    df = pd.DataFrame.from_records(biased_media_scores, columns=labels)
    df.to_csv('biased.csv')

    df = pd.DataFrame.from_records(unbiased_media_scores, columns=labels)
    df.to_csv('unbiased.csv')

if __name__ == "__main__":
    main()

