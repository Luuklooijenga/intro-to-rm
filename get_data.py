import pandas as pd
import numpy as np

# This file is for processing the raw data into usable data for our analysing script.
# The raw data was obtained from: https://www.kaggle.com/therohk/global-news-week/home#news-week-18aug24.csv
# for our research we used the news-week-18aug24.csv file.
#
# Media selection choices:
# Center: w3-usatoday, w3-bbc, w3-npr-org
# Left: w3-nydailynews, w3-nymag, w3-mashable
# Right: w3-breitbart, w3-nationalreview, w3-theblaze

def main():

    df = pd.read_csv("news-week-18aug24.csv")
    out_list = list()

    for i, row in df.iterrows():
        # Center
        if row[1] == "w3-usatoday":
            out_list.append([row[1], row[3]])    
        elif row[1] == "w3-bbc":
            out_list.append([row[1], row[3]])
        elif row[1] == "w3-npr-org":
            out_list.append([row[1], row[3]])
        # Left
        elif row[1] == "w3-nydailynews":
            out_list.append([row[1], row[3]])
        elif row[1] == "w3-mashable":
            out_list.append([row[1], row[3]])
        elif row[1] == "w3-nymag":
            out_list.append([row[1], row[3]])
        # Right
        elif row[1] == "w3-breitbart":
            out_list.append([row[1], row[3]])
        elif row[1] == "w3-nationalreview":
            out_list.append([row[1], row[3]])
        elif row[1] == "w3-theblaze":
            out_list.append([row[1], row[3]])

    with open("data.txt", "w") as outfile:
        for i in out_list:
            form_string = i[0] + "\t" + i[1] + "\n"
            outfile.write(form_string)

if __name__ == "__main__":
    main()
